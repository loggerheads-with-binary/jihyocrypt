from .. import key_maker , enc_b2b, dec_b2b, enc_f2f, dec_f2f 
import os 
import sqlite3 
import timeit 
import numpy as np 

import logging 
logger = logging.getLogger('stests')



DB = sqlite3.connect('speed.sqlite3')
DBCursor = DB.cursor()

KB , MB, GB  = 1024 , 1024 * 1024 , 1024* 1024 * 1024
buffer_sizes = [150*1024 , 1024*1024 , 1024*1024*10 , 1024*1024*100 , 1024*1024*1024, 1024*1024*1024*4 ]
files = {'stests/1.txt' : 150*KB , 'stests/2.txt' : MB, 'stests/3.txt' : 5*MB , 
         'stests/4.txt' : 10*MB , 'stests/5.txt' : 50*MB , 'stests/6.txt' : 100*MB } 
files2 =        {'stests/b%s.txt' % i : 100*(i+1)*MB for i in range(1,40) }
files3 =         {'stests/g1.txt' : 4*GB , 'stests/g2.txt' : 16*GB , 'stests/g3.txt' : 32*GB , 'stests/g4.txt' : 64*GB } 
bsizes = {1*MB , 2*MB , 5*MB , 10*MB , 100*MB , 1*GB , 2*GB}
files.update(files2)
files.update(files3)

if not os.path.isdir('stests'):
    os.makedirs('stests')

for key , value in files.items():

    if os.path.isfile(key):
        continue 

    with open(key , 'wb') as handle:
        for _ in range(value//8):
            handle.write('12345678')
    
    logger.info(f'Generated:: {key}')
    

##key maker tests 

DBCursor.execute("CREATE TABLE key_maker(counter INTEGER PRIMARY KEY , time_in_secs float(0) , jpic bool default true);")
for i in range(1 , 101):
    
    t = timeit.Timer(f'key_maker("12345678121212412" , rounds = {i} , redundant = True)')
    time_in_secs = np.mean(t.repeat(repeat = 3 ))            
    DBCursor.execute('INSERT INTO key_maker VALUES (? , ? , ?)' , (i , time_in_secs , True))
    
    t = timeit.Timer(f'key_maker("12345678121212412" , rounds = {i} , redundant = False)')
    time_in_secs = np.mean(t.repeat(repeat = 3 ))
    DBCursor.execute('INSERT INTO key_maker VALUES (? , ? , ?)' , (i , time_in_secs , False))
    logger.info(f'Completed Round {i}')

    
##enc b2b tests          
buffer_size = 3*MB
DBCursor.execute('CREATE TABLE enc_b2b_size(size INTEGER, time_in_secs FLOAT(0)')
for size in bsizes:
    DBCursor.execute('begin')
    data = os.urandom(size)
    t = timeit.Timer(f'enc_b2b(data , password = "12345678" , redundant = True)')
    l = t.autorange()
    time_in_secs = l[1]/l[0]    
    DBCursor.execute('commit')

size = 1*GB
DBCursor.executescript('begin;CREATE TABLE enc_b2b_buffer(buffer_size INTEGER, time_in_secs FLOAT(0), size INTEGER);commit') 
for __size in buffer_sizes:
    
    data = os.urandom(size)
    t = timeit.Timer(f'enc_b2b(data , password = "12345678" , redundant = True , buffer_size = __size)')
    l = t.autorange()
    time_in_secs = l[1]/l[0]
    DBCursor.execute('INSERT INTO enc_b2b_buffer VALUES (?,?,?)' , () )