import os, sys  
from .__init__ import SalsaMix, subroutine_major, BUFFER_SIZE, key_maker 
import warnings 
import _io 

ALLOWED_MODES = ('rb' , 'wb' , 'r+b' , 'w+b' , 'r' , 'w' , 'rt' ,'wt' , 'r+' , 'w+')

DEFAULT_FOPEN_ARGS = {      'buffering' : -1 , 
                            'encoding' : sys.getdefaultencoding() , 
                            'errors' : None , 
                            'newline' : None , 
                            'closefd' : True , 
                            'opener' : None }

class ModeError(TypeError):
    pass 

class PasswordError(ValueError):
    pass

class FileError(IOError):
    pass

class CorruptionError(IOError, ValueError):
    pass 

##Overload from documentation 
class Handle(_io.BufferedRandom):
    
    def __init__(self, filepath , mode ,  password  , redundant : bool = True ,  **kwargs ):
        
        if mode not in ALLOWED_MODES:
            raise ModeError(f'Invalid mode {mode} chosen, only modes {ALLOWED_MODES} are allowed') 
        
        self.filepath = filepath
        self.mode = mode 
        self.redundant = redundant 
        self.password = password
        self.file_kwargs = DEFAULT_FOPEN_ARGS.copy()
        self.file_kwargs.update(kwargs)
        
        self.encoding = self.file_kwargs.pop('encoding')       
        
        if 'r' in self.mode:
            if not os.path.isfile(filepath):
                raise FileNotFoundError(f'File {filepath} not found')
        
        self.mode_wise_open()
        
        if 'r' in mode:
            
            self.nonce = self.filehandle.read(8)
            
            if len(self.nonce) < 8:
                raise CorruptionError('Corrupted File')
            
            if redundant:
                self.verify()
            
        else:
            
            self.nonce = os.urandom(8)
            self.filehandle.write(self.nonce)
            
            if self.redundant:
                self.filehandle.write(key_maker(self.password,  self.nonce , redundant = True)[-1])
            
        if '+' in mode:
            self.ecipher = SalsaMix(self.password , self.nonce )
            self.dcipher = SalsaMix(self.password , self.nonce)
            
        elif 'w' in mode:
            self.ecipher = SalsaMix(self.password, self.nonce)
            
        elif 'r' in mode:
            self.dcipher = SalsaMix(self.password, self.nonce)     
    
        self.funcs_expose()
    
        return None                      

    def mode_wise_open(self):
        
        if self.mode in ('w' , 'wb' , 'rt'):
            self.filehandle = open(self.filepath , 'wb' , **self.file_kwargs)

        elif self.mode in ('r' , 'rb' , 'wt'):
            self.filehandle = open(self.filepath , 'rb' , **self.file_kwargs)
            
        elif self.mode in  ('r+' , 'r+b' , 'rb+'):
            self.filehandle = open(self.filepath , 'r+b' , **self.file_kwargs)
            
        elif self.mode in ('wb+' , 'w+' , 'w+b'):
            self.filehandle = open(self.filepath , 'w+b' , **self.file_kwargs)

    def funcs_expose(self):
        
        if 'b' in self.mode:
            self.__read = self.__b_read 
            self.__write = self.__b_write
        
        else:
            self.__read = self.__t_read 
            self.__write = self.__t_write
        
        if '+' in self.mode:
            self.read = self.__read 
            self.write = self.__write 
            
        elif 'r' in self.mode:
            self.read = self.__read 
            
        elif 'w' in self.mode :
            self.write = self.__write

    def verify(self):
        
        jpic = self.filehandle.read(512//8)
        
        if len(jpic) < 512//8:
            raise CorruptionError("Corrupted File given")
        
        if key_maker(self.password , self.nonce , redundant = True)[-1] != jpic:
            raise PasswordError('Incorrect Password Entered')  
    
    def __enter__(self):
        
        return self 
    
    def __b_read(self , counter : int = None ):
    
        return self.dcipher.decrypt(self.filehandle.read(counter))
    
    def __b_write(self, data : bytes):
        
        if 'b' not in self.mode:
            data = data.encode(self.encoding)
            
        return self.filehandle.write(self.ecipher.encrypt(data))
  
    def __t_read(self, counter : int = None):
        
        return self.__b_read(counter).decode(self.encoding)
    
    def __t_write(self , data : str):
        
        self.__b_write(self.data.encode(self.encoding))
             
    def __exit__(self):
        
        self.filehandle.close()
        return None 
    
    def close(self):
        
        self.filehandle.close()
        return None 